<?php

/**
 * Configure ctool plugin
 */
$plugin = array(
  'title' => t('T2 Demo pane'),
  'single' => TRUE,
  'category' => t('Decks'),
  'edit form' => 't2_demo_settings_form',
  'render callback' => 't2_demo_render_pane',
  'no title override' => TRUE,
  'defaults' => array(
    'foobar' => t('Demo text'),
  ),
  'all contexts' => TRUE,
);

/**
 * Render callback
 */
function t2_demo_render_pane($subtype, $conf, $args, $context){
  $resource_url = str_replace(DRUPAL_ROOT . DIRECTORY_SEPARATOR, '', dirname(__FILE__)) . '/resources';
  _t2_demo_pane_include_javascript($resource_url, $conf);
  _t2_demo_pane_include_style($resource_url);
  $markup = file_get_contents(DRUPAL_ROOT . DIRECTORY_SEPARATOR . $resource_url . DIRECTORY_SEPARATOR . 't2_demo_markup.html');
  $block = new stdClass();
  $block->content = $markup;
  return $block;
}

/**
 * Settings form callback.
 */
function t2_demo_settings_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $form['foobar'] = array(
    '#type' => 'textfield',
    '#title' => t('FooBar'),
    '#description' => t('Lorem ipsum'),
    '#default_value' => $conf['foobar'],
    '#required' => TRUE,
  );

  return $form;
}

function t2_demo_settings_form_submit(&$form, &$form_state) {
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    if (isset($form_state['values'][$key])) {
      $form_state['conf'][$key] = $form_state['values'][$key];
    }
  }
}

function _t2_demo_pane_include_javascript($resource_url, $conf){
  drupal_add_js(array('t2_demo' => array('resource_base' => $resource_url, 'foobar' => $conf['foobar'])), 'setting');
  drupal_add_js($resource_url . '/js/t2_demo.js', array('scope' => 'footer'));
}

function _t2_demo_pane_include_style($resource_url){
  drupal_add_css($resource_url . '/css/t2_demo.css');
  drupal_add_css($resource_url . '/css/t2_demo.ie.css', array('browsers' => array('IE' => 'lte IE 8','!IE' => FALSE)));
}
